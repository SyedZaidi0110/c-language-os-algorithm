/*
*  Assignment:
*  Mohsin Raza Zaidi 
*  Roll No MSCSF19M021
*  Programmer: Arif Butt
*  Course: System Programming with Linux
*  lsv4.c:
*  usage: ./a.out 
         ./a.out dirpath
         ./a.out dir1 dir2 dir3
	./a.out display in logn listing 
	display coloring 

*/

#include <sys/ioctl.h>
#include <stdio.h>
#include <unistd.h>
#include <dirent.h>
#include <string.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <time.h>
#include <grp.h>
#include <pwd.h>
#include <stdbool.h>
#include <getopt.h>
static char* global_dir = ".";
void show_stat_info(char*);
extern int errno;
//void do_ls(char*);

struct Options
{
    bool using_a;
    bool using_l;
    bool using_c;
    bool using_R;

};

static void init_opts(struct Options* opts)
{
    opts->using_a = false;
    opts->using_l = false;
    opts->using_c = false;
    opts->using_R = false;

}

struct Options get_opts(int count, char* args[])
{
    struct Options opts;
    init_opts(&opts);
    int opt;

    while ((opt = getopt(count, args, "alcR")) != -1)
    {
        switch (opt)
        {
            case 'a': opts.using_a = true; break;
            case 'l': opts.using_l = true; break;
            case 'c': opts.using_c= true; break;
            case 'R': opts.using_R = true; break;
	}
    }

    return opts;
}
/////////color function//////////
void get_color(char chr)
{
	if (chr =='p')
	printf( "\x1b[0;33m");
	else if (chr =='c')
	printf( "\x1b[1;33m");	
	else if (chr =='d')
	printf( "\x1b[1;34m");
	else if (chr =='b')
	printf( "\x1b[1;33m");
	else if (chr =='l')
	printf( "\x1b[1;3m");
	else if (chr =='s')
	printf( "\x1b[1;35m");
	else if (chr =='e')
	printf( "\x1b[1;32m");
	else
	printf( "\x1b[0;m");
}
int get_terminal_col()
{
	struct winsize w;

    ioctl(STDOUT_FILENO, TIOCGWINSZ, &w);
	int col = (w.ws_col/w.ws_row)*2;
	  printf ("columns caluculated: %d\n", w.ws_row);
	if( (w.ws_col%w.ws_row ) !=0) col++;
	printf ("columns caluculated: %d\n", col);
	return  col;
}
///////////// listing function////////////
void do_ls(char * dir, const struct Options opts)
{
   struct dirent * entry;
   DIR * dp = opendir(dir);
   if (dp == NULL){
      fprintf(stderr, "Cannot open directory:%s\n",dir);
      return;
   }
   chdir(dir);
   errno = 0;
   int column = get_terminal_col();
   int i=column;
  // printf ("columns caluculated: %d\n", column);
   while((entry = readdir(dp)) != NULL){
	
         if(entry == NULL && errno != 0){
  		perror("readdir failed");
		exit(1);
  	}else{
                if(entry->d_name[0] == '.')
                    continue;

        } 
	if(strcmp(entry->d_name , "-l")==0)
        continue;
	//printf("%s   ",entry->d_name); 
        if (opts.using_l) show_stat_info(entry->d_name);
        else{
	if (i==0 ) {printf("\n%s ",entry->d_name);i=column; }	
	else{printf("%s ",entry->d_name);
		}
		
	}	
	i--;
   }
	printf("\n");
   chdir("..");
   closedir(dp);
}
void scan_dirs_options(int argc, char* argv[], struct Options opts)
{
	  if (argc == 1)
	  {
          printf("Directory listing of pwd:\n");
    	  do_ls(".", opts);
 	  }
	  else if (argc == 2 && opts.using_l)
  	  {
            printf("Directory listing of pwd:\n");
    	    do_ls(".", opts);}
          else
	  {
       	   int i = 0;
           while(++i < argc){
	   if(strcmp(argv[i],"-l")==0) continue;
           printf("Directory listing of %s:\n", argv[i] );
	   do_ls(argv[i] ,opts);
           }
   }
}
int main(int argc, char* argv[]){
	scan_dirs_options(argc, argv, get_opts(argc, argv));
   
   return 0;
}


struct stat get_stats(const char* filename)
{
    char path[1024];
    sprintf(path, "%s/%s", global_dir, filename);
    struct stat sb;

    if (lstat(path, &sb) < 0)
    {   
        perror(path);
        exit(1);
    }
	
    return sb;
}


//// Permissions ///////////////////
void file_permissions(int mode, char str[])
{
 
   strcpy(str, "----------");
//owner  permissions
   if((mode & 0000400) == 0000400) str[1] = 'r';
   if((mode & 0000200) == 0000200) str[2] = 'w';
   if((mode & 0000100) == 0000100) str[3] = 'x';
//group permissions
   if((mode & 0000040) == 0000040) str[4] = 'r';
   if((mode & 0000020) == 0000020) str[5] = 'w';
   if((mode & 0000010) == 0000010) str[6] = 'x';
//others  permissions
   if((mode & 0000004) == 0000004) str[7] = 'r';
   if((mode & 0000002) == 0000002) str[8] = 'w';
   if((mode & 0000001) == 0000001) str[9] = 'x';
//special  permissions
   if((mode & 0004000) == 0004000) str[3] = 's';
   if((mode & 0002000) == 0002000) str[6] = 's';
   if((mode & 0001000) == 0001000) str[9] = 't';
 //  printf("\n%s ", str);
//return str;
}
////////////group name///////////
void get_grp_name(int gid)
{
  struct group * grp = getgrgid(gid);
   errno = 0;
   if (grp == NULL){
      if (errno == 0)
       printf("Record not found in /etc/group file.\t");
      else
          perror("getgrgid failed");
   }else
      printf("%s ", grp->gr_name);   
}
////////////group name///////////
void get_user_name(int uid)
{
errno = 0;
   struct passwd * pwd = getpwuid(uid);
  
   if (pwd == NULL){
      if (errno == 0)
         printf("Record not found in passwd file. ");
      else
         perror("getpwuid failed");
   }
   else
       printf("%s ",pwd->pw_name);   
}
/////////// function shows file type ////////////
char file_type(int st_mode)
{
   if ((st_mode &  0170000) == 0010000) 
	return 'p';	
	//printf("%s is a Named Pipe\n", argv[1]);
   else if ((st_mode &  0170000) == 0020000) 
	return 'c';
//	printf("%s is a Character Special file\n", argv[1]);
   else if ((st_mode &  0170000) == 0040000) 
	return 'd';
//	printf("%s is a Directory\n", argv[1]);
   else if ((st_mode &  0170000) == 0060000) 
	return 'b';
//	printf("%s is a Block Special file\n", argv[1]);
   else if ((st_mode &  0170000) == 0100000) 
	return '-';
//	printf("%s is a Regular file\n", argv[1]);
   else if ((st_mode &  0170000) == 0120000) 
	return 'l';
//	printf("%s is a Soft link\n", argv[1]);
   else if ((st_mode &  0170000) == 0140000) 
	return 's';
//	printf("%s is a Socket\n", argv[1]);
   else 
	return 'u';
}
////////////show stats of files///////////////
void show_stat_info(char *fname){
   struct stat info, sb;
   info = get_stats(fname);
   char str[11];
   file_permissions(info.st_mode, str);
   str[0] = file_type(info.st_mode);
   printf("%s ", str);
   printf("%ld ", info.st_nlink);
   get_user_name(info.st_uid);  
   get_grp_name(info.st_gid);
   printf("%7ld ", info.st_size);
   long secs = info.st_mtime;
   char * timestr = ctime(&secs);
   int len= strlen (timestr);
   timestr[len-1]= ' ';
   printf("%27s", timestr);
   if (stat(fname, &sb) == 0 && sb.st_mode & S_IXUSR) get_color('e'); 
   else get_color( str[0]); 
   printf("%s\n", fname );
   get_color('k'); 
}

